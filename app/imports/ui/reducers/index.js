import { combineReducers } from 'redux'
import authHandler from './authentication'
import loginHandler from './login'
import eventHandler from './event'

export default combineReducers({
    authHandler,
    loginHandler,
    eventHandler
})
