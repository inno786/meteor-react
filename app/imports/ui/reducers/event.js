import { ON_CHANGE_EVENT_FIELD, ON_VALIDATION_EVENT, ON_CHANGE_EVENT_MSG, LOAD_DATA } from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        title: '',
        description: ''
    },
    error: {
        title: '',
        description: ''
    },
    msg: ''
}

// Event Handler
const eventHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_EVENT_FIELD:
            state.field[action.field] = action.value
            return {
                ...state,
                field: state.field,
                error: state.error,
                msg: state.msg
            }
        case ON_VALIDATION_EVENT:
            state.error = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_EVENT_MSG:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case LOAD_DATA:
            return {
                ...state,
                field: Object.assign({}, action.payload)
            }
        default:
            return {
                ...state
            }
    }
}

export default eventHandler
