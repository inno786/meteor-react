import React from 'react';
import { connect } from 'react-redux'
import Login from '../../components/layout/Login'
import { onChange, validate, onUpdateMsg } from '../../actions/login'
import localStorage from '../../module/Auth'

class Content extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            userInfo: {
                username: 'admin',
                password: 'admin',
                name: "Admin"
            }
        }
        localStorage.deauthenticateUser('userInfo');
        this.onSubmit = this.onSubmit.bind(this)
    }
    onSubmit(event) {
        event.preventDefault()
        let username = this.props.arrData.field.username,
            password = this.props.arrData.field.password;
        this.props.validate(this.props.arrData.field);
        if (username && password) {
            if (username === this.state.userInfo.username && password === this.state.userInfo.password) {
                this.props.onUpdateMsg("Credintial is correct")
                localStorage.authenticateUser('userInfo', JSON.stringify(this.state.userInfo));
                this.props.history.push('/events');
            } else {
                this.props.onUpdateMsg("Invalid password")
            }
        } else {
            this.props.onUpdateMsg("Invalid username/password")
        }
    }
    /**
     * Render the component.
     */
    render() {
        return (
            <Login
                onSubmit={this.onSubmit}
                onChange={this.props.onChange}
                arrData={this.props.arrData}
            />
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        arrData: state.loginHandler
    }
}

export default connect(
    mapStateToProps,
    { onChange, validate, onUpdateMsg }
)(Content)