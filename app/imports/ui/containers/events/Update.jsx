import React from 'react';
import { connect } from 'react-redux'
import Layout from '../../components/layout/EventUpdate'
import { onChange, validate, onUpdateMsg, loadData } from '../../actions/event'
import localStorage from '../../module/Auth'
import { Events } from "../../../api/events";

class Update extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.match.params.id
        }
        this.onSubmit = this.onSubmit.bind(this)
    }
    componentDidMount() {
        const resp = Events.findOne({ _id: this.state.id });
        this.props.loadData(resp);
    }
    onSubmit(event) {
        event.preventDefault()
        this.props.validate(this.props.arrData.field);
        Events.update({ _id: this.state.id }, { $set: this.props.arrData.field }, (error, result) => {
            if (error) {
                console.error(error);
            } else {
                this.props.loadData({ title: '', description: '' });
                this.props.history.push('/events');
            }
        });
    }

    /**
     * Render the component.
     */
    render() {
        return (
            <Layout
                onSubmit={this.onSubmit}
                onChange={this.props.onChange}
                arrData={this.props.arrData}
            />
        );
    }
}
const mapStateToProps = (state, props) => {
    return {
        arrData: state.eventHandler
    }
}

const Content = connect(
    mapStateToProps,
    { onChange, validate, onUpdateMsg, loadData }
)(Update)
// export the component `App`
export default Content;
