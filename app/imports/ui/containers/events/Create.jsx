import React from 'react';
import { connect } from 'react-redux'
import Layout from '../../components/layout/EventCreate'
import { onChange, validate, onUpdateMsg, loadData } from '../../actions/event'
import localStorage from '../../module/Auth'
import { Events } from "../../../api/events";

class Create extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.onSubmit = this.onSubmit.bind(this)
    }
    onSubmit(event) {
        event.preventDefault()
        this.props.validate(this.props.arrData.field);
        const { title, description } = this.props.arrData.field;
        if (title && description) {
            // add method `insert` to db
            Events.insert({
                title,
                description
            });
            this.props.loadData({ title: '', description: '' });
            this.props.history.push('/events');
        } else {
            this.props.onUpdateMsg("Something is missing.")
        }
    }

    /**
     * Render the component.
     */
    render() {
        return (
            <Layout
                onSubmit={this.onSubmit}
                onChange={this.props.onChange}
                arrData={this.props.arrData}
            />
        );
    }
}
const mapStateToProps = (state, props) => {
    return {
        arrData: state.eventHandler
    }
}

export default connect(
    mapStateToProps,
    { onChange, validate, onUpdateMsg, loadData }
)(Create)
