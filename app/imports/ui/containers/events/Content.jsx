import React from 'react';
// we import withTracker and Events into our app file
import { withTracker } from 'meteor/react-meteor-data';
import { Events } from "../../../api/events";

import Layout from '../../components/layout/EventList'
import localStorage from '../../module/Auth'

class Content extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.deleteItem = this.deleteItem.bind(this);
    }
    deleteItem(id) {
        Events.remove({_id:id}, (error, result) =>{
            this.props.events = Events.find({}).fetch()  
        });
    }
    /**
     * Render the component.
     */
    render() {
        return (
            <Layout
                data={this.props.events}
                deleteItem={this.deleteItem}
            />
        );
    }
}
// Wrap `EventApp` with the HOC withTracker and call the new component we get `App`
const App = withTracker(() => {
    return {
        events: Events.find({}).fetch()
    }
})(Content);

// export the component `App`
export default App;
