import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import {
    Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import Form from '../events/Form'
import Header from './Header'

const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'center'
    },
    table_default: {
        width: '50%',
        border: '0px',
        align: 'center'
    },
    table_container: {
        width: '80%'
    },
    removePadding: {
        padding: '0px !important',
        whiteSpace: 'normal',
        wordWrap: 'break-word'
    }
}
const customContentStyle = {
    width: '100%',
    maxWidth: 'none',
}
const EventCreate = (props) => (
    <div style={styles.main}>
        <Header />
        <table style={styles.table_default} align="center">
            <tbody>
                <tr>
                    <td style={styles.table_container}>
                        <h2>Create new event</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <Form
                            onSubmit={props.onSubmit}
                            onChange={props.onChange}
                            arrData={props.arrData}
                        />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
)

export default EventCreate
