import React from 'react'
import { Link } from 'react-router-dom'
import FlatButton from 'material-ui/FlatButton';
import {
    Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import EventItem from '../events/EventItem'
import Header from './Header'

const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'center'
    },
    table_default: {
        width: '50%',
        border: '0px'
    },
    table_container: {
        width: '80%'
    },
    removePadding: {
        padding: '0px !important',
        whiteSpace: 'normal',
        wordWrap: 'break-word'
    }
}
const customContentStyle = {
    width: '100%',
    maxWidth: 'none',
}
const EventList = (props) => (
    <div style={styles.main}>
        <Header />
        <table style={styles.table_default}>
            <tbody>
                <tr>
                    <td style={styles.table_container}>
                        <h2>List of Events</h2>
                        <Link to='/event/create'>Create New Event</Link>

                    </td>
                </tr>
                <tr>
                    <td style={styles.table_container}>
                        <EventItem
                            items={props.data}
                            deleteItem={props.deleteItem}
                        />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
)

export default EventList
