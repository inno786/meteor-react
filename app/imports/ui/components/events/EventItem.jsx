import React from 'react';
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import {
    Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn
}
    from 'material-ui/Table';

const EventItem = ({
    items,
    deleteItem
}) => (
        <Table
            fixedHeader={true}
            multiSelectable={false}
            selectable={false}
        >
            <TableHeader
                enableSelectAll={false}
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    <TableHeaderColumn tooltip="Title">Title</TableHeaderColumn>
                    <TableHeaderColumn tooltip="Description">Description</TableHeaderColumn>
                    <TableHeaderColumn tooltip="Action">Action</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                {items.length > 0 && items.map((row, index) => (
                    <TableRow key={index}>
                        <TableRowColumn>{row.title}</TableRowColumn>
                        <TableRowColumn>{row.description}</TableRowColumn>
                        <TableRowColumn>
                            <Link to={`/event/update/${row._id}`}>
                                <img src='/img/edit_24x24.png' alt='edit' />
                            </Link>
                            <img src='/img/delete_24x24.png' onClick={() => { deleteItem(row._id) }} alt='delete' />
                        </TableRowColumn>
                    </TableRow>
                )
                )
                }
            </TableBody>
        </Table>
    );
export default EventItem;
