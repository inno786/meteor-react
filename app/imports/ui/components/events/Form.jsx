import React, { PropTypes } from 'react';
import { Card, CardActions } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

const styles = {
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        display: 'flex',
    },
    submit_btn: {
        marginTop: '15px',
        width: '20%'
    },
    hint: {
        textAlign: 'center',
        marginTop: '1em',
        color: '#ccc',
    },
    errorMsg: {
        color: 'red'
    }
};

const Form = ({
    onSubmit,
    onChange,
    arrData
}) => (
        <form action="/" onSubmit={onSubmit}>
            <div style={styles.form}>
                {arrData.msg && <p style={styles.errorMsg}>{arrData.msg}</p>}
                <div style={styles.input}>
                    <TextField
                        floatingLabelText="Title"
                        name="title"
                        errorText={arrData.error.title}
                        onChange={onChange}
                        value={arrData.field.title}
                    />
                </div>
                <div style={styles.input}>
                    <TextField
                        floatingLabelText="Description"
                        name="description"
                        errorText={arrData.error.description}
                        onChange={onChange}
                        value={arrData.field.description}
                    />
                </div>
                <CardActions>
                    <RaisedButton type="submit" label="Submit" primary style={styles.submit_btn} />
                </CardActions>
            </div>
        </form>
    );
export default Form;
