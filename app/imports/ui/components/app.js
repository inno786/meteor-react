import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import { Provider } from 'react-redux'

// Container load
import Login from '../containers/login/Content';
import EventList from '../containers/events/Content';
import EventCreate from '../containers/events/Create';
import EventUpdate from '../containers/events/Update';

// Auth token
import RequireAuth from '../containers/RequireAuth';

const App = (props) => {
    return (
        <Provider store={props.store}>
            <Router>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route exact path="/events" component={RequireAuth(EventList)} />
                    <Route exact path="/event/create" component={RequireAuth(EventCreate)} />
                    <Route exact path="/event/update/:id" component={RequireAuth(EventUpdate)} />
                    <Route render={() => <h1>Page not found</h1>} />
                </Switch>
            </Router>
        </Provider>
    );
}
export default App;
