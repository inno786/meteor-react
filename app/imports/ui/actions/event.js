import React from 'react';
import * as Redux from 'react-redux';
import * as types from '../constants/ActionTypes'

// Set profile
export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        field: event.target.name,
        value: event.target.value
    }))
}
const onChangeDispatch = (data) => ({
    type: types.ON_CHANGE_EVENT_FIELD,
    field: data.field,
    value: data.value
})
export const validate = (data) => (dispatch) => {
    let error = {
        title: '',
        description: ''
    },
        msg = '';
    if (!data.title) {
        error.title = 'Title cannot be blank!'
    }
    if (!data.description) {
        error.description = 'Description cannot be blank!'
    }
    if (data.title && data.description) {
        msg = ''
    }
    dispatch(onValidateDispatch(error, msg))
}
const onValidateDispatch = (error, msg) => ({
    type: types.ON_VALIDATION_EVENT,
    error: error,
    msg: msg
})
export const onUpdateMsg = (msg) => (dispatch) => {
    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_CHANGE_EVENT_MSG,
    msg: data.msg
});
export const loadData = (data) => (dispatch) => {
    dispatch({
        type: types.LOAD_DATA,
        payload:data
    })
}