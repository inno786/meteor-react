import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import configureStore from './../imports/ui/store';
import App from './../imports/ui/components/app';

const store = configureStore();

Meteor.startup(() => {
  ReactDOM.render((
    <MuiThemeProvider muiTheme={getMuiTheme()}>
      <App store={store} />
    </MuiThemeProvider>),document.getElementById('app')
);
});
