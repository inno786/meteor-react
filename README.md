About Meteor
===========================================
Meteor is an open source, fullstack javascript platform. This allows you to develop in one language. It has some really cool perks the best of which is:
<ul>
   <li>It provides complete reactivity; this means your UI reflects the actual state of the world with  little development effort.</li>
   <li>The server sends actual data not HTML, and then the client renders it.</li>
</ul>
Meteor supported Blaze for its view layers but after Meteor 1.2 it supported other JS frameworks i.e Angular and React.

Dependency:
===========================================
<pre>
  <code>
    <ol>
      <li>Mongodb latest</li>
      <li>Meteor</li>
    </ol>
  </code>
</pre>

Installation Meteor: OS X || Linux:
===========================================
curl https://install.meteor.com/ | sh

How to run
===========================================
Run following commands
<pre>
  <code>
    <ol>
      <li>cd app</li>
      <li>meteor npm install</li>
      <li>meteor</li>
    </ol>
  </code>
</pre>

Active URL
===============================================
<pre>
  <code>
    <ol>
      <li>http://localhost:3000</li>
      <li>Username: admin</li>
      <li>Password: admin</li>
    </ol>
  </code>
</pre>